set terminal svg enhanced background rgb 'white'
set output "Punto3-TCP_UDP-cwnd-ssthresh-tcp.svg"
set xlabel "Time(seconds)"
set ylabel "Segments"
set title "Congestion Window Plot"
plot "Punto3-TCP_UDP-20-cwnd.data" using 1:2 title "cwnd-2-0" with lines, \
"Punto3-TCP_UDP-20-ssth.data" using 1:2 skip 19 title "ssthresh-2-0" with lines, \
"Punto3-TCP_UDP-30-cwnd.data" using 1:2 title "cwnd-3-0"with lines, \
"Punto3-TCP_UDP-30-ssth.data" using 1:2 skip 19 title "ssthresh-3-0" with lines

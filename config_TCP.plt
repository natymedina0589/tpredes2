set terminal svg enhanced background rgb 'white'
set output "Punto2-TCP-cwnd-ssthresh-tcp.svg"
set xlabel "Time(seconds)"
set ylabel "Segments"
set title "Congestion Window Plot"
plot "Punto2-TCP-20-cwnd.data" using 1:2 title "cwnd-2-0" with lines, \
"Punto2-TCP-20-ssth.data" using 1:2 skip 25 title "ssthresh-2-0" with lines, \
"Punto2-TCP-30-cwnd.data" using 1:2 title "cwnd-3-0"with lines, \
"Punto2-TCP-30-ssth.data" using 1:2 skip 26 title "ssthresh-3-0" with lines
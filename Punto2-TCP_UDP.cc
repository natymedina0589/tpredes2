#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/netanim-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-layout-module.h"
#include "ns3/traffic-control-helper.h"
#include "ns3/flow-monitor-module.h"
#include "ns3/packet-sink.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "aux.h"

#include <iostream>

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("SOR2-dumbbell topology con TCP y UDP");

int main (int argc, char *argv[])
{

  bool tracing = true; // Enable tracing
  bool ENABLE_UDP = false;  // Enable udp node

  //configuraciones general
  Config::SetDefault ("ns3::OnOffApplication::PacketSize", UintegerValue(50));
  Config::SetDefault("ns3::TcpL4Protocol::SocketType", StringValue("ns3::TcpNewReno"));

  //Numero de nodos de hoja del lado izquierdo
  uint32_t leftLeaf = 3;

  //Numero de nodos de hoja del lado derecho
  uint32_t rightLeaf = 3;


  CommandLine cmd;
  cmd.AddValue ("ENABLE_UDP", "Enable/Disable UDP Node", ENABLE_UDP);
  cmd.Parse (argc,argv);

  std::string prefix_file_name = ENABLE_UDP ? "Punto3-TCP_UDP" : "Punto2-TCP";

  //PointToPoint ambos lados
  PointToPointHelper pointToPointLeaf;
  pointToPointLeaf.SetDeviceAttribute("DataRate", StringValue ("100kbps"));
  pointToPointLeaf.SetChannelAttribute("Delay", StringValue ("100ms"));
  pointToPointLeaf.SetQueue ("ns3::DropTailQueue", "MaxSize", StringValue ("10p"));


  //PointToPoint router central
  PointToPointHelper pointToPointRouterCentral;
  pointToPointRouterCentral.SetDeviceAttribute  ("DataRate", StringValue ("80kbps"));
  pointToPointRouterCentral.SetChannelAttribute ("Delay", StringValue ("100ms"));
  pointToPointRouterCentral.SetQueue ("ns3::DropTailQueue", "MaxSize", StringValue ("10p"));

 
  //Creo un dumbbell topology con la libreria Helper de ns3
  //Doc en la fuente del informe
  PointToPointDumbbellHelper dumbbell(
                                leftLeaf, pointToPointLeaf,
                                rightLeaf, pointToPointLeaf,
                                pointToPointRouterCentral);

  //Instalo el stack
  InternetStackHelper stack;
  dumbbell.InstallStack(stack);
 
  //Asigno direcciones de IP a cada nodo
  //10.1.1.0 -> nodos izquierdos
  //10.2.1.0 -> nodos derechos
  //10.3.1.0 -> nodos centrales
  dumbbell.AssignIpv4Addresses (Ipv4AddressHelper ("10.1.1.0", "255.255.255.0"),
                                Ipv4AddressHelper ("10.2.1.0", "255.255.255.0"),
                                Ipv4AddressHelper ("10.3.1.0", "255.255.255.0"));

  //Instalo on/off a los nodos
  //Configuracion para UDP
  int portUDP=1000;
  OnOffHelper onOffHelperUDP ("ns3::UdpSocketFactory", Address ());
  onOffHelperUDP.SetAttribute ("OnTime", StringValue ("ns3::ConstantRandomVariable[Constant=1000.0]"));
  onOffHelperUDP.SetAttribute ("OffTime", StringValue ("ns3::ConstantRandomVariable[Constant=0]"));
  Address sinkLocalAddresssUDP(InetSocketAddress (Ipv4Address::GetAny (), portUDP));
  PacketSinkHelper sinkUDP ("ns3::UdpSocketFactory", sinkLocalAddresssUDP);
 
  //Configuracion para TCP
  //creo un on/off helper para TCP
  int portTCP=1001;
  OnOffHelper onOffHelperTCP ("ns3::TcpSocketFactory", Address ());
  onOffHelperTCP.SetAttribute ("OnTime", StringValue ("ns3::ConstantRandomVariable[Constant=1000.0]"));
  onOffHelperTCP.SetAttribute ("OffTime", StringValue ("ns3::ConstantRandomVariable[Constant=0]"));
  Address sinkLocalAddresssTCP(InetSocketAddress (Ipv4Address::GetAny (), portTCP));
  PacketSinkHelper sinkTCP ("ns3::TcpSocketFactory", sinkLocalAddresssTCP);

  //Container de apps
  ApplicationContainer clientApps;

 //Ciclo los nodos y defino cual es TCP y cual es UDP
  for(uint32_t i=0; i< dumbbell.LeftCount(); i++) {
    if(i==2) {

        if(!ENABLE_UDP) continue;
        //Nodo con UDP
        std::cout << "Nodo UDP: " << i << std::endl;
        AddressValue remoteAddressUDP(InetSocketAddress(dumbbell.GetRightIpv4Address(i), portUDP));
        onOffHelperUDP.SetAttribute("Remote", remoteAddressUDP);
        clientApps.Add(onOffHelperUDP.Install(dumbbell.GetLeft (i)));
        clientApps=sinkUDP.Install(dumbbell.GetRight(i));
    }
    else {
      //Nodo con TCP
      std::cout << "Nodo TCP: " << i << std::endl;
      AddressValue remoteAddressTCP (InetSocketAddress(dumbbell.GetRightIpv4Address(i), portTCP));
      onOffHelperTCP.SetAttribute("Remote", remoteAddressTCP);
      clientApps.Add(onOffHelperTCP.Install(dumbbell.GetLeft(i)));
      clientApps=sinkTCP.Install(dumbbell.GetRight(i));
    }
  }
  
  // Flow monitor
  Ptr<FlowMonitor> flowMonitor;
  FlowMonitorHelper flowHelper;
  flowMonitor = flowHelper.InstallAll();
 
  //Start after sink y stop before sink
  clientApps.Start(Seconds(0.0));
  clientApps.Stop(Seconds(10.0));

  //Establece el cuadro delimitador para la animacion
  dumbbell.BoundingBox(1, 1, 100, 100);

  //Archivo XML para NetAnim
  AnimationInterface anim(prefix_file_name + ".xml");
 
  //Configura la simulacion real
  Ipv4GlobalRoutingHelper::PopulateRoutingTables();

  if (tracing)
    {
      pointToPointLeaf.EnablePcapAll (prefix_file_name, false);

      std::ofstream ascii;
      Ptr<OutputStreamWrapper> ascii_wrap;
      ascii.open ((prefix_file_name + "-ascii").c_str ());
      ascii_wrap = new OutputStreamWrapper ((prefix_file_name + "-ascii").c_str (),
                                            std::ios::out);
      stack.EnableAsciiIpv4All (ascii_wrap);

      Simulator::Schedule (Seconds (0.00001), &TraceCwnd20, prefix_file_name + "-20-cwnd.data");
      Simulator::Schedule (Seconds (0.00001), &TraceSsThresh20, prefix_file_name + "-20-ssth.data");
      Simulator::Schedule (Seconds (0.00001), &TraceRtt20, prefix_file_name + "-20-rtt.data");
      Simulator::Schedule (Seconds (0.00001), &TraceRto20, prefix_file_name + "-20-rto.data");
      Simulator::Schedule (Seconds (0.00001), &TraceInFlight20, prefix_file_name + "-20-inflight.data");

      Simulator::Schedule (Seconds (0.00001), &TraceCwnd30, prefix_file_name + "-30-cwnd.data");
      Simulator::Schedule (Seconds (0.00001), &TraceSsThresh30, prefix_file_name + "-30-ssth.data");
      Simulator::Schedule (Seconds (0.00001), &TraceRtt30, prefix_file_name + "-30-rtt.data");
      Simulator::Schedule (Seconds (0.00001), &TraceRto30, prefix_file_name + "-30-rto.data");
      Simulator::Schedule (Seconds (0.00001), &TraceInFlight30, prefix_file_name + "-30-inflight.data");
    }
 
  flowMonitor->CheckForLostPackets ();
  FlowMonitor::FlowStatsContainer stats = flowMonitor->GetFlowStats ();

  //Stop simulador
  Simulator::Stop(Seconds(100));
  
  //Run simulador
  Simulator::Run();

  flowMonitor->SerializeToXmlFile(prefix_file_name + "_flowmonitor.xml", true, true);

  //Destroy simulador
  Simulator::Destroy();
  return 0;
}
